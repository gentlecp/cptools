# ChangLog

## v.1.4.6
- `fix`: fix error when import `DingDing` in notify, `EmailSender` will raise error.

## v1.4.5
- `fix`: fix error in v.1.4.4

## ~~v1.4.4~~
> abandoned

- `logger`: add logger name in log display.
- `setup`: remove requirements for `DingtalkChatbot` and `yagmail` in `setup.py`
#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
-----------------File Info-----------------------
Name: __about__.py
Description: author and project info
Author: GentleCP
Email: me@gentlecp.com
Create Date: 2021/6/4 
-----------------End-----------------------------
"""
__name__ = "cptools"
__version__ = '1.4.6'
__author__ = 'GentleCP'
__author_email__ = 'me@gentlecp.com'
__description__ = 'Tools used by CP'
__url__ = 'https://github.com/GentleCP/cptools'